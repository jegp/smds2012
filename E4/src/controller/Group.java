package controller;

import model.Task;
import org.jgroups.JChannel;
import org.jgroups.Message;

/**
 * The connection to the JGroup.
 */
public class Group {

    private static JChannel channel;

    public static void init() {
        try {
            channel = new JChannel();
            channel.setReceiver(new TaskReceiver());
            channel.connect("TaskCluster");
        } catch (Exception e) {
            System.out.println("Error connecting");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void send(Command msg) {
        try {
            channel.send(new Message(null, null, msg));
        } catch (Exception e) {
            System.out.println("Error sending message");
            e.printStackTrace();
        }
    }
    
    public static void closeChannel() {
    	channel.close();
    }

    public static void main(String[] args) {
        TaskManager.addTask(new Task("sun01","Do homevork", "now", "not done", "just do it already", "me"));
    }

}
