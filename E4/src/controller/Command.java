package controller;

import model.Task;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: JensEgholm
 * Date: 15-10-12
 * Time: 16:41
 * To change this template use File | Settings | File Templates.
 */
public interface Command extends Serializable {

    public static class Create implements Command {
        public final Task task;
        public Create(Task task) {
            this.task = task;
        }
    }

    public static class Update implements Command {
        public final Task task;
        public Update(Task task) {
            this.task = task;
        }
    }

    public static class Delete implements Command {
        public final Task task;
        public Delete(Task task) {
            this.task = task;
        }
    }

}
