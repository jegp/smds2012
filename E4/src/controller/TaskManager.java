package controller;

import model.Task;
import org.jgroups.JChannel;

/**
 * A task manager that can
 */
class TaskManager {

    public static void addTask(Task task) {
        Group.send(new Command.Create(task));
    }

    public static void removeTask(Task task) {
        Group.send(new Command.Delete(task));
    }

    public static void updateTask(Task task) {
        Group.send(new Command.Update(task));
    }

}
