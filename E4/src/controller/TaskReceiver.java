package controller;

import java.util.List;

import model.Task;
import model.TaskList;

import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

import util.XMLUtility;

/**
 * A receiver for tasks.
 */
public class TaskReceiver extends ReceiverAdapter {
	
	private static TaskList taskList = XMLUtility.deserializeTaskList();
	
	public TaskReceiver() {
		taskList = XMLUtility.deserializeTaskList();
	}
	
	public void viewAccepted(View newView) {
	    System.out.println("Someone joined the group!");
	}

    @Override
    public void receive(Message msg) {
        Command c = (Command) msg.getObject();
        if (c instanceof Command.Create) {
        	Command.Create createCommand = (Command.Create) c;
            taskList.addTask(createCommand.task);
        } else if (c instanceof Command.Delete) {
        	Command.Delete deleteCommand = (Command.Delete) c;
            taskList.deleteTask(deleteCommand.task);
        } else if (c instanceof Command.Update) {
        	Command.Update updateCommand = (Command.Update) c;
            taskList.updateTask(updateCommand.task);
        } else {
            System.out.println("received unknown msg from " + msg.getSrc() + ": " + msg.getObject());
        }
    }
    
    public static List<Task> getTasks() {
    	return TaskList.tasks;
    }

}
