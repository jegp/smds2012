package util;

import model.Task;
import model.TaskList;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class XMLUtility {

    private static JAXBContext jaxbContext;
    private static final String path = "E4/resource/task-manager-xml.xml";

    /**
     * Utility class for writing and reading task xml from files
     */
    public XMLUtility() {
        // create an instance context class, to serialize/deserialize.
        try {
            jaxbContext = JAXBContext.newInstance();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read xml from a file, and return a taskmanager object containing the
     * deserialized data
     *
     * @param path path to the xml file
     * @return taskmanager containing the deserialized data
     * @throws FileNotFoundException the specified path could not be found
     */
    public static TaskList deserializeTaskList(){
        // Create a file input stream for the university Xml.
        FileInputStream stream;
        try {
        	jaxbContext = JAXBContext.newInstance(TaskList.class);
            stream = new FileInputStream(path);
            TaskList tm = (TaskList) jaxbContext.createUnmarshaller().unmarshal(stream);
            return tm;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLUtility.class.getName()).log(Level.SEVERE, null, ex);
            return new TaskList();
        } catch (JAXBException e) {
            System.out.println(e.toString());
            return new TaskList();
        }
    }

    /**
     * Write the contents of a taskmanager object to an xml file
     *
     * @param taskManager the taskmanager to write
     * @param path path to the file
     * @throws IOException
     */
    public static void serializeTaskList(TaskList taskList) {

        StringWriter writer = new StringWriter();

        // We can use the same context object, as it knows how to 
        //serialize or deserialize TaskList class.
        try {
            jaxbContext.createMarshaller().marshal(taskList, writer);
            // Finally save the Xml back to the file.
            serialize(writer.toString());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private static void serialize(String xml) {
        File file = new File(path);
        try {
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            output.write(xml);
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(XMLUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void saveTask(Task task) {
    	String xml = getTaskXml(task);
    	serialize(xml);
    }

    private static String getTaskXml(Task task) {

        try {
            StringWriter writer = new StringWriter();

            // create a context object for Student Class
            JAXBContext jaxbContext = JAXBContext.newInstance(Task.class);

            // Call marshal method to serialize student object into Xml
            jaxbContext.createMarshaller().marshal(task, writer);
            //System.out.println(writer);
            return writer.toString();

        } catch (JAXBException ex) {
            ex.printStackTrace();
            return "";
        }
    }

    private static Task getTaskFromXml(String xml) {
        JAXBContext context;
        try {
            context = JAXBContext.newInstance(Task.class);
            StringReader reader = new StringReader(xml);
            Task t = (Task) context.createUnmarshaller().unmarshal(reader);
            return t;
        } catch (Exception e) {
        	e.printStackTrace();
        	return null;
        }


    }
}
