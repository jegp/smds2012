package ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import model.Task;
import model.TaskList;
import controller.Command;
import controller.TaskReceiver;

import controller.Group;

public class TaskManagerConsole {
	private static InputStreamReader converter = new InputStreamReader(System.in);
	private static BufferedReader in = new BufferedReader(converter);
	
	public static void main(String[] args) {
		Group.init();
		while(true) {
			try {
				System.out.println("Choose operation:");
				System.out.println("1: Add task");
				System.out.println("2. Delete task");
				System.out.println("3. Update task");
				System.out.println("4. Print all tasks");
				System.out.println("5. Exit");
			
				int operation = Integer.parseInt(in.readLine());
				if (operation == 5) break;
				chooseOperation(operation);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Group.closeChannel();
	}
	
	private static void chooseOperation(int operation) {
		if (operation == 1) {
			Task t = readTask();
			Command.Create c = new Command.Create(t);
			Group.send(c);
		}
		else if (operation == 2) {
			Task t = deleteTask();
			Command.Delete c = new Command.Delete(t);
			Group.send(c);
		}
		else if (operation == 3) {
			Task t = readTask();
			Command.Update c = new Command.Update(t);
			Group.send(c);
		}
		else if(operation == 4) printTasks();
		else System.out.println("Choice not recognized");
	}
	
	private static void printTasks() {
		for (Task t : TaskReceiver.getTasks()) System.out.println(t);	
	}

	private static void updatetask() {
		// TODO Auto-generated method stub
		
	}

	private static Task deleteTask() {
		try {
		System.out.println("Enter ID:");
		String ID = in.readLine();
		return new Task(ID, "","","","","");
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Task readTask() {
		try {
		System.out.println("Enter ID:");
		String ID = in.readLine();
		System.out.println("Enter name:");
		String name = in.readLine();
		System.out.println("Enter date:");
		String date = in.readLine();
		System.out.println("Enter status:");
		String status = in.readLine();
		System.out.println("Enter description:");
		String description = in.readLine();
		System.out.println("Enter attendants;");
		String attendants = in.readLine();
		Task t = new Task(ID, name, date, status, description, attendants);
		return t;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
