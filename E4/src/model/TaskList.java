package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.jgroups.Message;

import util.XMLUtility;

/**
 * A list of Tasks stored in each client.
 */

@XmlRootElement(name = "cal")
public class TaskList {

	@XmlElementWrapper(name = "users")
	@XmlElement(name = "user")
	public static List<User> users;

	@XmlElementWrapper(name = "tasks")
	@XmlElement(name = "task")
	public static List<Task> tasks;
	
	public TaskList() {
		users = new ArrayList<User>();
		tasks = new ArrayList<Task>();
	}

	public void addTask(Task task) {
		tasks.add(task);
		XMLUtility.serializeTaskList(this);
		System.out.println("Task added");
	}
	
	public void deleteTask(Task task) {
		Iterator<Task> i = tasks.iterator();
		while (i.hasNext()) {
			Task t = i.next();
			if (t.id.equals(task.id)) i.remove();
		}
		XMLUtility.serializeTaskList(this);
		System.out.println("Task deleted");
	}
	
	public void updateTask(Task task) {
		deleteTask(task);
		tasks.add(task);
		XMLUtility.serializeTaskList(this);
		System.out.println("Task updated");
	}
	
	public static List<Task> getTasks() {
		return tasks;
	}

}
