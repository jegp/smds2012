package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
public class User {
	@XmlElement(name = "name")
	public String name;
	@XmlElement(name = "password")
	public String password;
	
	public User(String name, String password) {
		this.name = name;
		this.password = password;
	}
	
	public User() {
		name = "";
		password = "";
	}

}
