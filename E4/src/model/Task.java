package model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "task")
public class Task implements Serializable {
	
	@XmlAttribute(name = "id")
	public String id;
	@XmlAttribute(name = "date")
	public String date;
	@XmlAttribute(name = "status")
	public String status;
	@XmlAttribute(name = "name")
	public String name;
	@XmlElement(name = "description")
	public String description;
	@XmlElement(name = "attendants")
	public String attendants;
	
	
	public Task(String ID, String name, String date, String status,
				String description, String attendants) {
		
		this.id = ID;
		this.date = date;
		this.description = description;
		this.name = name;
		this.attendants = attendants;
		this.status = status;
	}
	
	public Task() {
		id = "";
		date = "";
		status = "";
		name = "";
		description = "";
		attendants = "";
	}
	
	@Override
	public String toString() {
		return "ID: " + id + "\nDate: " + date + "\nDescription: " + description + "\nName: " + name + "\nAttendants: " + attendants + "\n"; 
	}
}